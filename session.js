// node session server

var http = require('http');
var url = require('url');
var fs = require('fs');

var server = "gateway_address_here";
var webAppId = "webapp-id-example";
var localPort = 8080;

var serverUrl = "https://" + server + ":8443/gateway";


var generatedUser = 1;

var handleRequest = function(request, response) {
    var urlParams = url.parse(request.url, true).query;
    var username = urlParams.userId;

    // Check if this is a request for the page icon
    if ((/^\/favicon\.ico/).test(request.url)) {
        console.log('Favicon requested, not generating session');
        response.writeHead(204);
        response.end();
        return;
    }

    // Redirect with a generated user if one was not provided
    if (!username || username.length === 0) {
        console.log("No username provided, redirecting");
        response.writeHead(302, {
            Location: '?user=user' + generatedUser
        });
        generatedUser++;
        response.end();
        return;
    }

    console.log("Processing request, user [" + urlParams.user + "]");

    var sessionDesc = {
        "webAppId": webAppId,
        "allowedOrigins": ["*"],
        "urlSchemeDetails": {
            "host": server,
            "port": "8443",
            "secure": true
        },
        "voice":
        {
            "username": username,
            "displayName": username,
            "domain": server,
            "inboundCallingEnabled": true
        },
        "aed": {
            "accessibleSessionIdRegex": ".*",
            "maxMessageAndUploadSize": "5000",                                                                                       
            "dataAllowance": "5000"                                                                   
        },
            "additionalAttributes": {
                "AED2.metadata": {
                    "role":"consumer"
                },
            "AED2.allowedTopic": username
        }
    };

    var sessionDescJson = JSON.stringify(sessionDesc);

    var options = {
        hostname: server,
        port: 8080,

        path: '/gateway/sessions/session',
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            'Content-Length':sessionDescJson.length,
            'Access-Control-Allow-Origin' : '*'
        }
    };

    var processSessionResponse = function(sessionResponse) {
        sessionResponse.setEncoding('utf-8');
        sessionResponse.on('data', function(chunk) {
            console.log(chunk);
            var sessionId = JSON.parse(chunk).sessionid;

            response.setHeader('Access-Control-Allow-Origin', '*');
            response.setHeader('Access-Control-Request-Method', '*');
            response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
            response.setHeader('Access-Control-Allow-Headers', '*');

            response.end(sessionId, 'utf-8');
        });
    };

    var sessionRequest = http.request(options, processSessionResponse);

    sessionRequest.on('error', function(e) {
        console.log("Error: " + e.message);
    });

    sessionRequest.end(JSON.stringify(sessionDesc), 'utf-8');
};

http.createServer(handleRequest).listen(localPort);
console.log('Server running on port: [' + localPort + ']');
console.log('Server running at http://127.0.0.1:8080/');