##Separate voice & Video from LA Co-browse

###QuickStart

Simple app to separate voice & video from live Assist whilst still implementing co-browse.

1. A CBA FAS & FCSDK / Live Assist server <gateway_address_here>
2. To edit the html files to point to the IP of your CBA server and session provisioning script <session_provisioning_server_address_here>.
3. Run the client-side code locally on port 8000
4. USe the sample agent console with agent1 to recieve the call and initiate co-browse
