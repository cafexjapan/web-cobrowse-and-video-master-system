$(function () {
  // hook up the hangup button
  $("#hangup").click(function () {
    $("#videoPane").addClass("hidden");
    localStorage.clear();
    window.controlWindow.hangup();
    AssistSDK.endSupport();
    window.controlWindow.close();
  });

  if (
    localStorage.getItem("la-video-coords") &&
    localStorage.getItem("la-started")
  ) {
    var cuurentCoords = JSON.parse(localStorage.getItem("la-video-coords"))[
      "videoCoords"
    ];
    console.log({ cuurentCoords });

    window.controlWindow = window.open(
      "",
      "control_window",
      "width=0,height=0"
    );
    console.log({ controlWindow });

    $("#videoPane").removeClass("hidden");
    
    // set existing pane cords
    $("#videoPane").offset({
      top: cuurentCoords.top,
      left: cuurentCoords.left,
    });

    window.controlWindow.postMessage(
      JSON.stringify({
        videoCoords: {
          top: cuurentCoords.pageY,
          left: cuurentCoords.pageX,
        },
      }),
      "http://localhost:8000/control.html" // change this depending on where this app is running
    );
  }

  window.attachAgentStream = function (stream, coords) {
    console.log("attachAgentStream ", stream);
    console.log("attachAgentStream ", coords);

    if ($("#videoPane").hasClass("hidden")) {
      $("#videoPane").removeClass("hidden");
    }

    if (coords) {
      // set existing pane cords
      $("#videoPane").offset({
        top: coords.top,
        left: coords.left,
      });
    }

    document.getElementById("remote").srcObject = stream;
  };

  $("#startup").click(function () {
    window.controlWindow = window.open(
      "control.html",
      "control_window",
      "left=1000,top=100,width=320,height=320"
    );

    window.addEventListener(
      "message",
      (event) => {
        console.log("postMessage revieved from control", event.data);

        var sessionToken = JSON.parse(event.data)["sessionToken"];
        var cid = JSON.parse(event.data)["cid"];
        console.warn({ sessionToken });
        console.warn({ cid });

        if (sessionToken && cid) {
          console.log("AssistSDK startSupport");
          AssistSDK.startSupport({
            correlationId: cid,
            sessionToken: sessionToken,
          });
        }
      },
      false
    );

    window.controlWindow.postMessage(
      "hello there!",
      "http://localhost:8000/control.html" // change this depending on where this app is running
    );

    localStorage.setItem("la-started", "true");
  });

  // videoPane
  var $dragging = null;

  $(document.body).on("mousemove", function (e) {
    if ($dragging) {
      $dragging.offset({
        top: e.pageY,
        left: e.pageX,
      });

      window.controlWindow.postMessage(
        JSON.stringify({
          videoCoords: {
            top: e.pageY,
            left: e.pageX,
          },
        }),
        "http://localhost:8000/control.html" // change this depending on where this app is running
      );

      localStorage.setItem(
        "la-video-coords",
        JSON.stringify({
          videoCoords: {
            top: e.pageY,
            left: e.pageX,
          },
        })
      );
    }
  });

  $(document.body).on("mousedown", "div", function (e) {
    // console.log(e.target);
    if (e.target.id === "videoPane") {
      $dragging = $(e.target);
    }
  });

  $(document.body).on("mouseup", function (e) {
    $dragging = null;
  });
});
